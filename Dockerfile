FROM klakegg/hugo:0.85.0-ext-ubuntu as builder

WORKDIR /app
COPY . .
RUN hugo

FROM nginx:stable-alpine

COPY --from=builder /app/public /usr/share/nginx/html
COPY nginx /etc/nginx/