---
title: 'Frequently Asked Questions about Pantavisor Linux'
date: 2018-11-28T15:14:39+10:00
draft: false
weight: 6
description: In here you'll find answers to some frequently asked questions about the Pantavisor Linux embedded Linux container framework.
---

In here you'll find answers to some frequently asked questions about the Pantavisor Linux container framework.

- [How do I delete apps from device?](#delete-apps) 
- [My device is not connecting. How do I fix it?](#device-connecting)
- [How are Docker containers handled by Pantavisor Linux?](#docker-pantavisor)
- [Why is there an edit and an edit-config command?](#two-edits) 
- [What is the `_config` directory?](#overlay) 
- [What is the pvr command line interface?](#pvr-cli)



#### <a name="delete-apps"></a> Q: How do I delete apps from my device?
Unfortunately Pantabox currently doesn't have a UI command for uninstalling apps.  To uninstall an app type `pantabox` and then select edit. Select the app to uninstall and remove its files from the device with: 

```sh
rm -rf container_folder
pvr add .
pvr commit
exit 0
```
Do the same for any `_config` overlay folders and file you may have by selecting edit-config and your app from the dialog appears. Then remove the overlay app directories from the `_config` directory.

Once the system has installed the new revision, your app will not appear in the installed modules dialog. 

#### <a name="device-connecting"></a>Q: My device is not connecting.</summary>
There are two different methods for connecting a device to the Internet. The first is through a regular ethernet cable either attached directly to the board or through a shared connection on your laptop. The second method to connect is via WiFi and a captive portal.

If you are already connected via an ethernet cable, and you are not connecting to the network, reboot the device with `pantabox` --> `reboot` and wait a few minutes before logging in again. 

See [Setting up an Internet connection to your board](https://docs.pantahub.com/first-boot-rpi3/#set-up-internet-connection-to-your-board).


#### <a name="docker-pantavisor"></a>Q: How are Docker containers handled by Pantavisor Linux?
Pantavisor is an [LXC based runtime](https://linuxcontainers.org/lxc/introduction/) and does not run Docker directly as the default container format. We do however see that the developer tool ecosystem of Docker is extensive and convenient. Pantavisor therefor closes the gap of "How can I build LXC containers very easily" by providing a Docker like experience for creating Pantavisor containers.

It's important to note that we don't run Docker images directly with the docker engine. Pantavisor does use dockerfiles to define the root file system of a container because it's a very useful tool for this (see [Installing Apps from Docker Hub](/guides/install-from-docker-hub/). With dockerfiles, Pantavisor can generate an image that is the same as any image found in Docker Hub. The same image however is not run by Pantavisor. Whenever a new revision is created with pvr, we extract the root file system from the image and compress it with `squashfs` so that Pantavisor only runs that `rootfs` on LXC.

#### <a name="two-edits"></a>Q: Why is there an edit and an edit-config command?
Pantavisor makes a separation between editing files inside the container as opposed to editing files outside of the container. Even though both methods affect the configuration settings of the containers, the difference is in the portability of the underlying container. Editing meta-data rebuilds the containers with the new configuration whereas creating an overlay will keep the original container intact. 

Editing the container meta-data through `.json` files is done with the `edit` command.  But when editing the `_config` overlay (that exists outside of the container as a wrapper), it is created and modified with the `edit-config` command.  Both of these commands bring up interactive shells in which to work.   

See [Installing and configuring apps from Docker](/guides/install-from-docker-hub/)

#### <a name="overlay"></a> Q: What is the _config directory?
This is a directory that lives outside of any application containers running with Pantavisor.  It's main purpose is to specify configuration variables for the container that override those settings inside of the container. With the config wrapper you can easily specify different parameters for a particular use case without having to rebuild the container itself.  The directory structure and files mirror those inside the container and  are composed with the interactive shell that is invoked with the `edit-config` command in pantabox.  See [Why is there an edit and an edit-config command](/guides/faq#two-edits) and [Installing and configuring apps from Docker](/guides/install-from-docker-hub/).

#### <a name="pvr-cli"></a> Q: What is the PVR command line interface?
PVR is the command line utility for Pantavisor. It lets you interact with your Pantavisor-enabled device remotely through Pantahub. The CLI can be used to manage revisions, clone your device as well as post different device profiles to Pantacor Hub for easy testing and seamless onboarding, among other operations.


#### Questions or Comments? Join the Pantavisor Slack community
Have any questions or comments?  Please join our Slack community at [http://pantavisor.slack.com](https://pantavisor.slack.com).

