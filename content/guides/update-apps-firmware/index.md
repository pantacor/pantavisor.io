---
title: 'Managing App and Firmware Lifecycles with Pantavisor Linux'
date: 2018-11-28T15:14:39+10:00
draft: false
weight: 5
description: Manage IoT app and firmware lifecycles on any embedded Linux devices with Pantabox and Pantavisor Linux. 
---

Update any app container running in your system, including the containerized embedded system and framework built with Pantavisor Linux. With the Pantabox utilities, the `update` command downloads the most recent version of the container as specified by the source URL and tag in the `src.json` file.

**Before you begin**:

- [Pantavisor Linux should be installed and running on your device](/guides/getting_started#download-install)
- [A working network connection](https://docs.pantahub.com/first-boot-rpi3/#set-up-internet-connection-to-your-board) 
- [ssh connection onto the device](/guides/getting-started/#ssh-login)


For example: 

###### 1. Run `pantabox` to display the menu and then select `update` 
###### 2. Select the BSP (Board Support Package). 

The BSP or Board Support Packages are composed of the Linux Kernel, drivers and firmware. It also contain the Pantavisor binary that is responsible for orchestrating containers and managing updates. All of these components are updated on-demand with the `update` command. To read more about how Pantavisor Linux is architected, see, <a href="https://docs.pantahub.com/pantavisor-architecture/" target = "_blank"> "Pantavisor Architecture" </a>

<div class="d-flex justify-content-center mt-4 mb-4">
  <img src="/images/update-bsp-pantavisor.png" alt="update pantavisor" style="width:400px;">
</div>

Pantavisor updates the containers, and then applies the updates to the firmware, kernel and drivers and then reboots the device. Pantavisor <a href="https://docs.pantahub.com/pantavisor-state-format-v2/" target="_blank"> keeps all states and will always you can revert </a> back to a previous revision if needed. 

<div class="d-flex justify-content-center mt-4 mb-4">
  <img src="/images/install-objects-pantavisor.png" alt="update pantavisor" style="width:400px;">
</div>

**See Also:**
- [Installing an app from a Docker container](/guides/getting_started#install-apps-market)
- [Configuring and Installing Docker Hub Apps with Pantabox and Pantavisor Linux](/guides/install-from-docker-hub/)
- [Finding the IP to ssh onto a device](/guides/getting_started#ssh-login)


#### Questions or Comments? Join the Pantavisor community & forum
Have any questions or comments?  Please join our community forum at [Pantavisor Community Forum](https://community.pantavisor.io).
