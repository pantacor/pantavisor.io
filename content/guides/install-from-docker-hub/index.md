---
title: 'Configuring and Installing Docker Hub Apps with Pantabox and Pantavisor Linux'
draft: false
weight: 4
description: Download and install an app from the Docker Hub with Pantavisor. Docker containers don't run natively, but do use the root file systems for Docker containers.
---

In this guide you'll download and install an app from the [Docker Hub ecosystem](https://hub.docker.com/). Pantavisor doesn't run Docker containers natively, but it does use Docker images as a source of root file systems for containers run by the Pantavisor with LXC. You'll install the open source web server and load balancer [NGINX](https://www.nginx.com/) container onto the device and then change the default port setting using a Pantavisor `_config` overlay.   

Pantavisor Linux runs standard Docker containers, but in order to customize the configuration, you'll need to create a Pantavisor `_config` overlay that is managed by the Pantavisor system state format. Config overlays provide a simple way to manage custom configuration across device fleets while maintaining container portability. 

**Sections in this guide**: 

- [Download and install the app from Docker Hub](#download-docker-hub)
- [Apply device state changes](#apply-changes)
- [Adding the _config overlay](#add-config-overlay)
- [Changing the NGINX default port settings](#change-config-setting)
- [edit vs edit-config commands](#edit-vs-config)

**Before you begin**:

- [Pantavisor Linux should be installed and running on your device](/guides/getting_started#download-install)
- [A working network connection](https://docs.pantahub.com/first-boot-rpi3/#set-up-internet-connection-to-your-board) 
- [ssh connection onto the device](/guides/getting-started/#ssh-login)


## <a name="download-docker-hub"></a>Download and install NGINX from Docker Hub

To begin: 

###### 1. Display the pantabox menu.

Display the menu with the `pantabox` command and select `Install`. Pantabox can install apps from the following three sources:

- **Pantacor One App Marketplace** - [Free marketplace](https://one.pantacor.com) for Pantavisor apps.
- **Custom Source** - Any device or device hub either hosted by Pantacor or hosted on-premise.
- **Docker Hub** - Standard Docker images from the app ecosystem. Images are installed and converted with Pantavisor to run as LXC containers on your devices.

Select Docker Hub: 

<p class="d-flex justify-content-center mt-4 mb-4">
    <img src="/images/install-app-docker.png" alt="install app pantabox" style="width:400px;">
</p>

###### 2. Enter the Docker app name and the tag: 

```sh 
NGINX:latest
```

## <a name="apply-changes"></a>Applying device state changes

Once NGINX is finished downloading, you'll be taken to the Pantabox interactive shell where you will see the container files. The shell is an environment for editing metadata key-value pairs on any of the `.json` files. Changes to the .JSON files are saved as state revisions using the extensive and 'git-like' <a href="https://docs.pantahub.com/pvr/" target="_blank"> PVR command line </a> utilities.  See <a href="https://docs.pantahub.com/pvr/#the-state-json" target="_blank"> The State JSON format </a> for more information. 

<p class="d-flex justify-content-center mt-4 mb-4">
    <img src="/images/interactive-shell.png" alt="pantabox interactive shell" style="width:300px;">
</p>

For help on commands run `pvr -help`. Read more about what PVR can do in the <a href="https://docs.pantahub.com/pvr/" target="_blank">Official PVR reference guide</a>.  

You may want to revisit this interactive shell at a later stage to edit the container .JSON files, but for the moment, exit the interactive shell with `exit 0` and then apply your changes with `yes`.

## <a name="add-config-overlay"></a>Adding the _config overlay
Changing or adding configuration parameters for Dockerized apps require a `_config` overlay directory structure that exists outside of the container. The overlay directory structure and name of the configuration file must map to the same sub-directory and config file name found inside of the Docker container. In Pantavisor, the `_config` overlay for a container always takes precedence over any of the config settings found inside of the container. 

## <a name="change-config-setting"></a>Changing the NGINX default port settings
In this section, you'll change the default port setting for NGINX. Perhaps you already have an app on the device that runs on port :80? If this is the case, you must change the default port for NGINX to run on something other than :80. 

To do this: 

###### 1. Display the interactive shell

Display the menu with `pantabox` selecting `edit-config` and then `nginx`. This displays an interactive shell that allows you to edit the NGNIX `_config` overlay: 

<p class="d-flex justify-content-center mt-4 mb-4">
    <img src="/images/edit-config-shell.png" alt="pantabox interactive shell" style="width:300px;">
</p>

######  2. Create a new NGINX configuration

NGINX port settings are kept in the default.conf located in the `_config/nginx/etc/nginx/conf.d/` directory. So in this case, you can add those directories to the `_config` with:

```sh
mkdir -p _config/nginx/etc/nginx/conf.d/
```

###### 3. Create the config file with:

```sh
vi _config/nginx/etc/nginx/conf.d/default.conf
```

Then add the following to change the port from :80 to :8000: 

```txt
server {
    listen       8000;
    listen  [::]:8000;
    server_name  localhost;
    #access_log  /var/log/nginx/host.access.log  main;
    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }
    #error_page  404              /404.html;
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
```

###### 4. Add and commit your changes before exiting the shell with: 

```sh
pvr add .
pvr commit
```

and finally apply your changes with: 

```sh
exit 0
```

###### 5. View your changes 

View the default NGINX page by pointing your browser at the device's IP (found in the upper right hand corner of Pantabox) with port 8000. 

<p class="d-flex justify-content-center mt-4 mb-4">
    <img src="/images/nginx-default.png" alt="pantabox interactive shell" style="width:400px;">
</p>

## <a name="edit-vs-config"></a>edit vs edit-config commands
You may be wondering why there are two separate edit commands in Pantabox. Pantavisor has this notion of editing inside the container and editing outside of the container. When editing the container meta-data through `.json` files, you use the `edit` command.  But when editing the overlay that exists outside of the container as a wrapper, the `edit-config` command is implemented.  Both of these commands launch an interactive shell that allow you to save revisions.   

**See Also:**
 - [Configuring and Installing Docker Hub Apps with Pantabox and Pantavisor Linux](/guides/getting_started#install-apps-market)

#### Questions or Comments? Join the Pantavisor community & forum
Have any questions or comments?  Please join our community forum at [Pantavisor Community Forum](https://community.pantavisor.io).
