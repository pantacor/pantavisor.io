---
title: 'Install Pantavisor Linux onto your embedded Linux device'
draft: false
weight: 3
description: Flash and boot Pantavisor onto your embedded Linux device, run Pantabox, then download and run Home Assistant from a container.
summary: If you are just starting out, this guide is the place to start. It describes how to flash and boot up your device with Pantavisor Linux. You'll then download the Home Assistant app from the Pantacor One Marketplace and display it in your browser.
---

Pantavisor Linux comes installed with Pantabox which provides all of the tools to configure, install and manage your apps and even the kernel. In this guide, you'll learn how to flash and boot an embedded device with Pantavisor Linux and then run Pantabox to configure your device and any apps running on it. You'll then download and install the [Home Assistant](https://www.home-assistant.io/), an open source home automation platform from the Pantacor One Marketplace and display it in your browser. 

After completing this guide, you may want to try [Configuring and Installing Apps from Docker Hub](/guides/install-from-docker-hub/) where we dive a little deeper and describe how to use configuration overlays and a number of other features of Pantavisor Linux. 

**Sections in this guide**:

- [Download Pantavisor Linux and prepare the device](#download-install)
- [How to find the IP and ssh onto the device](#ssh-login)
- [Install Home Assistant from the marketplace](#install-apps-market)

**Before you begin**

You'll need to have available: 

- An Internet connection
- A laptop and a microSD card

## <a name="download-install"></a>Download and install Pantavisor Linux

###### 1. Download the base Pantavisor image.

Select the platform and architecture that matches your device and <a href="https://www.pantavisor.io/#download-image" target="_blank"> download the image</a>.

###### 2. Prepare the embedded Linux device.

Flash the microSD card with the downloaded bootable image. If you haven't done this before, refer to these instructions:

- <a href="https://docs.pantahub.com/image-setup-rpi3/#flash-initial-image" target="_blank"> Raspberry Pi</a>.
- <a href="https://docs.pantahub.com/image-setup/" target="_blank"> Other platforms </a>

###### 3. Boot device with Pantavisor Linux image.

Insert the microSD card into the device, ensure that it is connected to the network and then boot with the image. This procedure may differ depending on your platform: 

- <a href="https://docs.pantahub.com/first-boot-rpi3/" target="_blank"> Raspberry Pi </a>
- <a href="https://docs.pantahub.com/first-boot-rpi3/" target="_blank"> Other platforms </a>

###### 4. Connect device to the Internet

Devices can be connected to the Internet either through a WiFi captive or directly with an ethernet cable. For details on how to do that for your platform see <a href="https://docs.pantahub.com/first-boot-rpi3/#set-up-internet-connection-to-your-board" target="_blank"> Setting up an Internet Connection </a>

###### 5. Login with user: `root` and password: `pantabox`. 

After booting the device, if a monitor and keyboard is attached, you will see the Pantavisor Linux logo with some default instructions on how to login. 

##### <a name="ssh-login"></a> How to ssh on to the device
You can directly login to the device if you have a keyboard and monitor attached to it. But the more convenient way is to ssh on it:   

```sh
ssh root@[device-ip]
```
You can find the IP address of you device in the upper right hand corner after you've logged in. Login with the default `pantabox` password. (Setup your public key with the `edit-sshkeys` commands.)  

If you don't have a keyboard and monitor attached to log in and view the device's IP, then please [download the Pantavisor command line interface](https://docs.pantahub.com/install-pvr/) or `pvr` cli onto your laptop. Afterwards scan the network with:

```sh
pvr device scan
```
Note the IP address for your device and use that to ssh.  

**Important!** Before going into production, please remember to change the default login with the `chpasswd` command. 

###### 6. Display the menu with the `pantabox` command.

<p class="d-flex justify-content-center mt-4 mb-4">
    <img src="/images/pantabox-menu.png" alt="pantabox menu" style="width:300px;">
</p>

## <a name="install-apps-market"></a>Install Home Assistant from Pantacor One Marketplace

Now you're ready to install Home Assistant (an open source, privacy first Home Automation platform): 

###### 1. Navigate to the Pantacor One Marketplace. 

Display the pantabox menu with `pantabox` and then select `install`. Choose Pantacor One Marketplace: 

<p class="d-flex justify-content-center mt-4 mb-4">
    <img src="/images/install-app-pantabox.png" alt="install app pantabox" style="width:400px;">
</p>

###### 2. Select [Home Assistant](https://www.home-assistant.io/) from the list of available apps and press OK.

###### 3. View the app. 

Once the app is finished installing, run `pantabox` and select `View`: 

<p class="d-flex justify-content-center mt-4 mb-4">
    <img src="/images/installed-modules-pantabox.png" alt="install app pantabox" style="width:300px;">
</p>

###### 4. Locate the default port for Home Assistant. 

To locate the port setting for Home Assistant, type `pantabox` and then select `edit` from the menu. The Interactive Shell appear, where you can use `vi` or `nano` to view the app manifest: 

```sh
vi homeassistant/v0alpha.app.manifest.json
``` 
###### 5. Add and commit your changes before exiting the shell. 

This creates a local device revision that includes the newly installed Home Assistant app. 

```sh
pvr add .
pvr commit
```

Apply your changes with: 

```sh
exit 0
```
###### 6. Display the Admin dashboard. 

Finish setting up Home Assistant by pointing your browser at the IP address of the device (located in the right hand corner of your screen) with the port specified in the app manifest: 

<p class="d-flex justify-content-center mt-4 mb-4">
    <img src="/images/home-assistant-login.png" alt="Home Assistant admin screen" style="width:400px;">
</p>


**See Also**:
 - [Configuring and Installing Docker Hub Apps with Pantabox and Pantavisor Linux](/guides/install-from-docker-hub/)
 - <a href="https://docs.pantahub.com/first-boot-rpi3/#set-up-internet-connection-to-your-board" target="_blank">Setting up your Internet connection to Your Board </a>


#### Questions or Comments? Join the Pantavisor community & forum
Have any questions or comments?  Please join our community forum at [Pantavisor Community Forum](https://community.pantavisor.io).
