
const PlatformNamesTranslate = {
  "bpi-r2": "Banana PI R2",
  "bpi-r64": "Banana PI 64",
  "malta": "Malta",
  "odroid-c2": "Odroid C2",
  "rpi0w": "Raspberry Pi Zero W",
  "rpi2": "Raspberry PI 2",
  "rpi3": "Raspberry PI 3 (32 bits)",
  "rpi4": "Raspberry PI 4 (32 bits)",
  "rpi64": "Raspberry PI 3/4 (64 Bits)",
  "rpi64-5.10.y": "Raspberry PI 3/4 64 Bits (Kernel 5.10)",
  "x64-uefi-installer": "x64 UEFI System installer",
  "x64-uefi": "x64 UEFI System",
}

const platformWhitelist = {
  "bpi-r2": false,
  "bpi-r64": false,
  "odroid-c2": true,
  "rpi0w": true,
  "rpi2": false,
  "rpi3": false,
  "rpi4": false,
  "rpi64": true,
  "rpi64-5.10.y": true,
  "x64-uefi-installer": true,
  "x64-uefi": true,
}

const platformBlacklist = {
  "build-bpi_r64_openwrt_stable": true
}

function SlugCapitalize(s) {
  return s.split('-').reduce((acc, part) => {
    return `${acc} ${part[0].toUpperCase()}${part.substring(1, part.length)}`
  }, '')
}

function getOS() {
  const userAgent = navigator.userAgent.toLowerCase();
  if (userAgent.indexOf("win") >= 0) return "windows";
  if (userAgent.indexOf("mac") >= 0) return "macos";
  if (userAgent.indexOf("linux") >= 0) return "linux";
  return "Unknown OS";
}

function omit(obj, toOmit) {
  return Object.keys(obj).reduce((acc, key) => {
    if (toOmit.indexOf(key) < 0) {
      acc[key] = obj[key]
    }
    return acc
  }, {})
}

function getDowndownClick(event) {
  if (!event.target.classList.contains("dropdown-toggle")) return;
  const dropdown = event.target.parentElement.getElementsByClassName(
    "dropdown-menu"
  )[0];
  dropdown.classList.toggle("show");
}

function defaultDownloadVersion() {
  const dropdown = document.getElementById("download-pi-imager");
  if (!dropdown) {
    return;
  }
  const select = dropdown.getElementsByClassName("btn-select")[0];
  if (!select) {
    return;
  }
  const base = "Download for";
  if (!select) {
    return;
  }

  switch (getOS()) {
    case "windows":
      select.href = "https://downloads.raspberrypi.org/imager/imager_1.6.2.exe";
      select.innerHTML = `${base} Windows`;
      return;
    case "macos":
      select.href = "https://downloads.raspberrypi.org/imager/imager_1.6.2.dmg";
      select.innerHTML = `${base} Mac OS`;
      return;
    case "linux":
      select.href = "https://downloads.raspberrypi.org/imager/imager_1.6.2_amd64.deb";
      select.innerHTML = `${base} Ubuntu (.deb)`;
      return;
  }
}

function getDevicesImage(channelDevices, src = 'https://s31mg.svc.pantahub.com/pv-initial-devices/stable.json') {
  return new Promise((resolve, reject) => {
    fetch(src)
      .then(resp => resp.json())
      .then(MapImagesLinks)
      .then(resolve)
      .catch(reject)
  })
}

function Load() {
  defaultDownloadVersion();
  document.addEventListener("click", getDowndownClick);
}

function MapImagesLinks(json) {
  return Object.keys(json).reduce((acc, key) => {
    if (json[key].length > 0) {
      acc.channels.push(key)
      json[key][0].devices.forEach(d => {
        d.images.forEach(i => {
          const deviceID = `${d.devicenick}--${i.name}`
          acc.devices[deviceID] = {
            ...(acc.devices[deviceID] || {}),
            ...omit(d, ['images']),
            ...i,
            [key]: i.url
          }
        })
      })
    }
    return acc
  }, { channels: [], devices: {} })
}

function ShowImagesLinks(images) {
  const elem = document.getElementById("download-image")
  elem.innerHTML = `
  <div class="download-image-header hide-xs row pb-2 pt-2">
    <div class="col-sm-3">
      <b>Platform</b>
    </div> 
    <div class="col-sm-4">
      <b>Hub Device</b>
    </div> 
    <div class="col-sm-2">
      <b>Arch</b>
    </div>
    <div class="col-sm-3">
    </div>
  </div>
  `
  Object.keys(images.devices).forEach((key) => {
    const device = images.devices[key]

    const deviceName = device.name !== 'default'
    ? `${device.platform}-${device.name}`
    : device.platform

    console.log(deviceName)
    if (device.url === "" || !platformWhitelist[deviceName] || platformBlacklist[device.job]) {
      return
    }

    const newElem = document.createElement("div")
    const downloads = images.channels.sort((a, b) => a < b).reduce((acc, channel) => {
      if (!device[channel]) { return acc }
      return `
        ${acc}
        <a class="btn btn-primary btn-sm" href="${device[channel]}">
          ${SlugCapitalize(channel)}
        <a>
      `
    }, ``)
    newElem.className = "image-download-link row pb-2 pt-2"
    newElem.innerHTML = `
      <div class="col-sm-3 mt-1">
        <b class="show-xs">Platform: <br/></b>${PlatformNamesTranslate[deviceName] || deviceName}
      </div> 
      <div class="col-sm-4 mt-1">
        <b class="show-xs">Hub Device: <br/></b>${device.devicenick}
      </div> 
      <div class="col-sm-2 mt-1">
        <b class="show-xs">Arch: <br/></b>${device.target}
      </div>
      <div class="col-sm-3 mt-1">
        ${downloads}
      </div>
    `

    elem.appendChild(newElem)
  })
}

getDevicesImage().then(ShowImagesLinks)